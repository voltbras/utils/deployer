local kube = import 'kube.libsonnet';

{
  // shmSize is the shared memory size
  Stateful(name, ports, env, image, replicas=1, shmSize=256): {
    local volumeName = name+'-volume',

    apiVersion: 'v1',
    kind: 'List',
    container:: kube.Container(name, ports, env) {
      image: image,
      
      // hide probes, as postgres doesn't 
      // have health check endpoints
      readinessProbe:: super.readinessProbe,
      livenessProbe:: super.livenessProbe,

      volumeMounts: [{
        name: volumeName,
        mountPath: '/bitnami/postgresql/data',
        subPath: name
      }, {
        name: 'dshm',
        mountPath: '/dev/shm'
      }]
    },
    items: [
      kube.Deployment(name, self.container, replicas) {
        kind: 'StatefulSet',
        spec+: {
          serviceName: name,
          template+: {
            spec+: {
              initContainers: [{
                command: ["sh", "-c", "chown -R 1001:1001 /bitnami/postgresql/data"],
                image: "busybox",
                name: "changeowner",
                volumeMounts: [{
                  name: volumeName,
                  mountPath: '/bitnami/postgresql/data',
                  subPath: name
                }]
              }],
              volumes: [{
                name: 'dshm',
                emptyDir: { medium: 'Memory', sizeLimit: shmSize+'Mi' },
              }]
            },
          },
          volumeClaimTemplates: [
            {
              metadata: { name: volumeName },
              spec: {
                accessModes: ['ReadWriteOnce'],
                volumeMode: 'Filesystem',
                resources: { requests: { storage: '8Gi' }}
              }
            }
          ]
        },
      },
      kube.Service(name, self.container),
    ]
  },
  PostgresMaster(dbName, name, user, password, replUser, replPassword, replicas=1):
    $.Stateful(name+'-master', [5432], {
      POSTGRESQL_USERNAME: user,
      POSTGRESQL_PASSWORD: password,
      POSTGRESQL_DATABASE: dbName,
      POSTGRESQL_REPLICATION_MODE: 'master',
      POSTGRESQL_REPLICATION_USER: replUser,
      POSTGRESQL_REPLICATION_PASSWORD: replPassword,
      ALLOW_EMPTY_PASSWORD: 'yes',
  }, 'docker.io/bitnami/postgresql:11-debian-10', replicas),
  
  PostgresSlave(dbName, name, user, password, replUser, replPassword, replicas=1):
    $.Stateful(name+'-slave', [5432], {
      POSTGRESQL_USERNAME: user,
      POSTGRESQL_PASSWORD: password,
      POSTGRESQL_DATABASE: dbName,

      POSTGRESQL_REPLICATION_MODE: 'slave',
      POSTGRESQL_REPLICATION_USER: replUser,
      POSTGRESQL_REPLICATION_PASSWORD: replPassword,
      POSTGRESQL_MASTER_HOST: name+'-master',
      POSTGRESQL_MASTER_PORT_NUMBER: '5432',
      ALLOW_EMPTY_PASSWORD: 'yes',
    }, 'docker.io/bitnami/postgresql:11-debian-10', replicas),
  PostgresPool(dbName, name, user, password, masterReplicas, slaveReplicas): {
    local postgres = self,
    local containerName = name+'-pool',
    local configName = containerName+'-config',
    apiVersion: 'v1',
    kind: 'List',
    container:: kube.Container(containerName, [9999], {
      PGPOOL_PARAMS_BACKEND_HOSTNAME0: name+'-master',
      PGPOOL_PARAMS_BACKEND_FLAG0: "ALWAYS_PRIMARY|DISALLOW_TO_FAILOVER",
      PGPOOL_PARAMS_BACKEND_WEIGHT0: std.toString(masterReplicas),

      PGPOOL_PARAMS_BACKEND_HOSTNAME1: name+'-slave',
      PGPOOL_PARAMS_BACKEND_FLAG1: "DISALLOW_TO_FAILOVER",
      PGPOOL_PARAMS_BACKEND_WEIGHT1: std.toString(slaveReplicas),
      
      // number of connections
      PGPOOL_PARAMS_NUM_INIT_CHILDREN: '100',

      PGPOOL_PARAMS_SR_CHECK_PERIOD: '10',
      PGPOOL_PARAMS_SR_CHECK_DATABASE: dbName,
      PGPOOL_PARAMS_SR_CHECK_USER: user,
      PGPOOL_PARAMS_SR_CHECK_PASSWORD: password,

      PGPOOL_PARAMS_ENABLE_POOL_HBA: 'on',
      PGPOOL_PARAMS_LOAD_BALANCE_MODE: 'on',
      PGPOOL_PARAMS_STATEMENT_LEVEL_LOAD_BALANCE: 'on',
    }) {
      image: 'pgpool/pgpool:4.2',
      
      // hide probes, as postgres doesn't 
      // have health check endpoints
      readinessProbe:: super.readinessProbe,
      livenessProbe:: super.livenessProbe,

      volumeMounts: [{
        name: configName,
        mountPath: '/usr/local/pgpool-II/etc/pool_passwd',
        subPath: 'pool_passwd'
      }]
    },
    items: [
      {
        apiVersion: 'v1',
        kind: 'ConfigMap',
        metadata: {
          name: configName,
          labels: { name: configName },
        },
        data: {
          pool_passwd: user+":md5"+std.md5(password+user),
        },
      },
      kube.Deployment(containerName, postgres.container) {
        spec+: {
          template+: {
            spec+: {
              volumes: [{
                name: configName,
                configMap: {
                  name: configName,
                },
              }],
            },
          },
        },
      },
      kube.Service(containerName, postgres.container),
    ]
  },

  ReplicatedPostgres(dbName, name, user, password, replUser, replPassword, replicas=2): {
    local masterReplicas = 1,
    local slaveReplicas = replicas - masterReplicas,
    master: $.PostgresMaster(dbName, name, user, password, replUser, replPassword, masterReplicas),
    slaves: $.PostgresSlave(dbName, name, user, password, replUser, replPassword, slaveReplicas),
    pool: $.PostgresPool(dbName, name, user, password, masterReplicas, slaveReplicas),
  },
  ReplicatedPostgresDatabaseUrl(dbName, name, user, password): "postgres://"+user+":"+password+"@"+name+"-pool:9999/"+dbName,
}