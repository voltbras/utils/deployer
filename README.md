# ci-runner

how to test deploy stage locally:

```bash
git add . && gitlab-runner exec docker deploy-staging --env CI_DEPLOY_USER=<your user> --env CI_DEPLOY_PASSWORD=<your password> --env CI_REGISTRY=registry.gitlab.com --env KUBE_NAMESPACE=<namespace> --env CI_REGISTRY_IMAGE=registry.gitlab.com/voltbras/<project path> --env CI_ENVIRONMENT_NAME=<environment> --docker-volumes ~/.kube/config:/root/.kube/config --docker-network-mode=host
```
