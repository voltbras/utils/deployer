local ingress = import "../ingress.libsonnet";

{
  url: ingress('example', [{
    port: 4000,
    serviceName: 'example-service',
    host: 'example.com'
  }, {
    port: 4000,
    serviceName: 'example-service',
    host: '*.example.com'
  }], httpsRedirect=false)
}
