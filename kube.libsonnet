{
  Container(name, ports, env, healthCheckEndpoint='/.well-known/apollo/server-health'): {
    name: name,
    image: name,
    imagePullPolicy: 'Always',

    envList(map):: [
      if std.type(map[x]) == 'object' then { name: x, valueFrom: map[x] } else { name: x, value: map[x] }
      for x in std.objectFields(map)
    ],

    env: self.envList(env) + [{
      name: 'POD_NAME',
      valueFrom: { fieldRef: { fieldPath: 'metadata.name' },},
    }, {
      name: 'K8S_NAMESPACE',
      valueFrom: { fieldRef: { fieldPath: 'metadata.namespace' },},
    }],

    ports: [{containerPort: port} for port in ports],
    readinessProbe: {
      httpGet: {path: healthCheckEndpoint, port: ports[0],},
      periodSeconds: 30,
      successThreshold: 1,
      timeoutSeconds: 15,
    },
    livenessProbe: self.readinessProbe {
      initialDelaySeconds: 30,
    },
  },
  Deployment(name, container, replicas=1): {
    local deployment = self,
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: {
      name: name,
      labels: { name: name },
      annotations: {},
    },

    spec: {
      template: {
        metadata: {
          labels: deployment.metadata.labels,
          annotations: {},
        },
        spec: {
          containers: [container],
          imagePullSecrets: [{name: 'gitlab-registry'}],
        },
      },

      selector: {
        matchLabels: deployment.spec.template.metadata.labels,
      },

      replicas: replicas,
    },
  },
  Service(name, container): {
    local service = self,
    port:: container.ports[0].containerPort,
    apiVersion: 'v1',
    kind: 'Service',
    metadata: {
      name: name,
      labels: { name: name },
      annotations: {},
    },

    spec: {
      selector: { name: container.name },
      ports: [
        {
          port: service.port,
          targetPort: service.port,
        },
      ],
      type: 'ClusterIP',
    },
  },
  Postgres(name, user, password): {
    local postgres = self,
    local volumeName = 'postgresdb',

    apiVersion: 'v1',
    kind: 'List',
    container:: $.Container(name, [5432], {
      POSTGRES_USER: user,
      POSTGRES_PASSWORD: password
    }) {
      image: 'postgres:9.6.2-alpine',
      
      // hide probes, as postgres doesn't 
      // have health check endpoints
      readinessProbe:: super.readinessProbe,
      livenessProbe:: super.livenessProbe,

      volumeMounts: [{
        name: volumeName,
        mountPath: '/var/lib/postgresql/data',
        subPath: name
      }]
    },
    items: [
      $.Deployment(name, postgres.container) {
        kind: 'StatefulSet',
        spec+: {
          serviceName: name,
          volumeClaimTemplates: [
            {
              metadata: { name: volumeName },
              spec: {
                accessModes: ['ReadWriteOnce'],
                volumeMode: 'Filesystem',
                resources: { requests: { storage: '8Gi' }}
              }
            }
          ]
        },
      },
      $.Service(name, postgres.container)
    ]
  },
  // by default runs everyday at 4AM
  // pass a dbUrl and the backupName to be made in Google Cloud Storage
  PostgresBackupCron(name, dbUrl, backupName, gcpProjectId, dbBackupServiceAccountJson=null, schedule="0 4 * * *"): {
    apiVersion: "batch/v1beta1",
    kind: "CronJob",
    metadata: {
      name: name + "-db-backup"
    },
    spec: {
      schedule: schedule,
      jobTemplate: {
        spec: {
          template: {
            spec: {
              containers: [
                {
                  name: name + "-db-backup-job",
                  image: "voltbras/psql-gsutil",
                  command: ["/bin/sh", "-c"],
                  args: [|||
                    echo "$DB_BACKUP_SERVICE_ACCOUNT_JSON" > key.json
                    gcloud auth activate-service-account --key-file=key.json
                    gcloud config set project %(gcpProjectId)s
                    pg_dump %(dbUrl)s | gsutil cp - gs://voltbras-db-backups%(backupName)s-dump-$(date -Iseconds).sql
                  ||| % {
                    dbUrl: dbUrl,
                    backupName: backupName,
                    gcpProjectId: gcpProjectId
                  }],
                  env: [
                    { name: 'DB_BACKUP_SERVICE_ACCOUNT_JSON', value: dbBackupServiceAccountJson }
                  ],
                }
              ],
              restartPolicy: "OnFailure"
            }
          }
        }
      }
    }
  },
}
